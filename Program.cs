﻿using System;

//Esercizio 3:
//Creare un programma che contenga due funzioni chiamate TrovaMinimo() e TrovaMassimo(), che rispettivamente trovano il numero minimo e quello massimo tra due numeri passati come parametri.
//Aggiungere poi due varianti di queste funzioni, rendendole di tipo void e facendo restituire il risultato modificando un terzo parametro(ref/out)



namespace TrovaMinimo
{
    class Program
    {
        static void Main(string[] args)
        {
            int primoNumero;
            int secondoNumero;
            int risultato = 0;
            int minoreMaggiore;
          

            Console.WriteLine("Buongiorno, inserisci due numeri,ti riferirò il numero minimo e massimo");
            Console.WriteLine("Il primo numero è :");
            primoNumero = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Mentre il secondo numero è :");
            secondoNumero = Convert.ToInt32(Console.ReadLine());
            minoreMaggiore = TrovaMinimo(primoNumero, secondoNumero);
            Console.WriteLine("Il numero minore tra i {0} e {1} è il :", primoNumero, secondoNumero);
            Console.WriteLine(minoreMaggiore);
            minoreMaggiore = TrovaMassimo(primoNumero, secondoNumero);
            Console.WriteLine("Mentre il numero maggiore tra {0} e {1} è il : ", primoNumero, secondoNumero);
            Console.WriteLine(minoreMaggiore);
            TrovaMinimoTipoDue(primoNumero, secondoNumero, ref risultato); // quando passo per riferimento nn posso salvare in una variabile 
            Console.WriteLine("Il numero minore trovato con la seconda funzione è : ");
            Console.WriteLine(risultato);
            TrovaMassimoTipoDue(primoNumero, secondoNumero, out risultato);
            Console.WriteLine("Mentre il numero maggiore trovato con la seconda funzione è : ");
            Console.WriteLine(risultato);

        }

        static int TrovaMinimo(int uno,int due)
        {
            if (uno > due)
            {
                return due;
            }
            else 
            {
                return uno;
            }
        }

        static int TrovaMassimo(int uno, int due)
        {
            if (uno > due)
            {
                return uno;
            }
            else
            {
                return due;
            }

        }

        static void TrovaMinimoTipoDue(int uno,int due,ref int risultato)
        {
            if (uno > due)
            {
                risultato = due;
            }
            else
            {
                risultato = uno;
            }

        }

        static void TrovaMassimoTipoDue(int uno,int due,out int risultato)
        {
            
            if (uno > due)
            {
               risultato = uno;
            }
            else
            {
                risultato = due;
            }
        }


    }
}
